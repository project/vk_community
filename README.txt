CONTENTS OF THIS FILE
----------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Features


INTRODUCTION
------------

The VK Community provides a block 100% configurable basead on 
https://vk.com/dev/Community


REQUIREMENTS
------------

No requirements.

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. See:
     https://drupal.org/documentation/install/modules-themes/modules-8
     for further information.


CONFIGURATION
-------------
 * Access the page https://vk.com/dev/Community to find the group id of the 
 page you want to render.

 * With the group id, go to Block Layout and enable the VK Community block.
 * Go to VK Communit configuration and fill the form.

FEATURES
--------
 * The VK Community is translatable. Its mean, you can render a different 
 account for any language.

MAINTAINERS
-----------

Current maintainers:

 * Julio Hidemi Kamizato (https://www.drupal.org/u/jkamizato)
